﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FortCode.Data.Entities;
using FortCode.Models;

namespace FortCode.Services
{
    public interface ISecretBarClubService
    {
        Task<CityModel> AddFavoriteCity(CityModel city, int userId);
        Task<int> DeleteFavoriteCity(int cityId, int userId);
        Task<List<CityModel>> GetFavoriteCities(int userId);
        Task<CityModel> GetFavoriteCity(int cityId, int userId);
        Task<List<BarModel>> GetFavoriteBars(int cityId, int userId);
        Task<BarModel> GetFavoriteBar(int barId, int userId);
        Task<BarModel> AddFavoriteBar(BarModel bar, int cityId, int userId);
        Task<int> DeleteFavoriteBar(int barId, int userId);
        Task<List<SharedBarsModel>> GetFavoriteBarsShared(int userId);
        Task<UserModel> UserLogin(UserModel user);
        Task<UserModel> GetUserByEmail(string email);
        Task<UserModel> AddNewUser(UserModel user);
    }
}
