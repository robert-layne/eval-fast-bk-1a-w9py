﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using AutoMapper;
using FortCode.Data;
using FortCode.Data.Entities;
using FortCode.Models;
using Microsoft.EntityFrameworkCore;

namespace FortCode.Services
{
    // todo: break this out into smaller classes (e.g. cities, bars, users)... for now leaving all in one
    public class SecretBarClubService : ISecretBarClubService
    {
        private readonly IMapper _mapper;
        private readonly SecretBarClubContext _secretBarClubContext;

        public SecretBarClubService(IMapper mapper, SecretBarClubContext secretBarClubContext)
        {
            _mapper = mapper;
            _secretBarClubContext = secretBarClubContext;
        }

        public async Task<CityModel> AddFavoriteCity(CityModel city, int userId)
        {
            var newCity = _mapper.Map<City>(city);
            newCity.UserId = userId;

            await _secretBarClubContext.Cities.AddAsync(newCity);
            await _secretBarClubContext.SaveChangesAsync();

            return _mapper.Map<CityModel>(newCity);
        }

        public async Task<int> DeleteFavoriteCity(int cityId, int userId)
        {
            _secretBarClubContext.Cities.Remove(new City { Id = cityId });
            await _secretBarClubContext.SaveChangesAsync();

            return cityId;
        }

        public async Task<List<CityModel>> GetFavoriteCities(int userId)
        {
            var cities = await _secretBarClubContext.Cities
                .AsNoTracking()
                .Where(x => x.User.Id == userId)
                .OrderBy(x => x.Name)
                .ToListAsync();

            var model = _mapper.Map<List<CityModel>>(cities);

            return model;
        }

        public async Task<CityModel> GetFavoriteCity(int cityId, int userId)
        {
            var cities = await _secretBarClubContext.Cities
                .AsNoTracking()
                .SingleOrDefaultAsync(x => x.User.Id == userId &&
                             x.Id == cityId);

            var model = _mapper.Map<CityModel>(cities);

            return model;
        }

        public async Task<List<BarModel>> GetFavoriteBars(int cityId, int userId)
        {
            var bars = await _secretBarClubContext.Bars
                .AsNoTracking()
                .Where(x => x.City.Id == cityId)
                .ToListAsync();

            var model = _mapper.Map<List<BarModel>>(bars);

            return model;
        }

        public async Task<BarModel> GetFavoriteBar(int barId, int userId)
        {
            var cities =  _secretBarClubContext.Cities
                .AsNoTracking()
                .Include(x => x.Bars)
                .Where(x => x.User.Id == userId);

            var bar = await cities.SelectMany(x => x.Bars)
                .SingleOrDefaultAsync(x => x.Id == barId);

            var model = _mapper.Map<BarModel>(bar);

            return model;
        }

        public async Task<BarModel> AddFavoriteBar(BarModel bar, int cityId, int userId)
        {
            var newBar = _mapper.Map<Bar>(bar);
            newBar.CityId = cityId;

            await _secretBarClubContext.Bars.AddAsync(newBar);
            await _secretBarClubContext.SaveChangesAsync();

            return _mapper.Map<BarModel>(newBar);
        }

        public async Task<int> DeleteFavoriteBar(int barId, int userId)
        {
            _secretBarClubContext.Bars.Remove(new Bar { Id = barId });
            await _secretBarClubContext.SaveChangesAsync();

            return barId;
        }

        public async Task<List<SharedBarsModel>> GetFavoriteBarsShared(int userId)
        {
            // todo: this method is bad, refactor entities so we can query this in one nice statement using keys or something else
            
            var myCities = await _secretBarClubContext.Cities
                .AsNoTracking()
                .Include(x => x.Bars)
                .Where(x => x.User.Id == userId)
                .ToListAsync();

            var otherCities = await _secretBarClubContext.Cities
                .AsNoTracking()
                .Include(x => x.Bars)
                .Where(x => x.User.Id != userId)
                .ToListAsync();

            var results = new List<SharedBarsModel>();

            foreach (var myCity in myCities)
            {

                var othersBars = otherCities.Where(x => x.Name.ToUpperInvariant() == myCity.Name.ToUpperInvariant() &&
                                                        x.Country.ToUpperInvariant() == myCity.Country.ToUpperInvariant())
                    .SelectMany(x => x.Bars)
                    .Select(x => x.Name)
                    .Distinct()
                    .ToList();

                var myBars = myCity.Bars.GroupBy(x => x.Name).Select(x => x.First());

                foreach (var myBar in myBars)
                {
                    var sharedBars = othersBars.Where(x => x.ToUpperInvariant() == myBar.Name.ToUpperInvariant()).ToList();

                    if (sharedBars.Any())
                    {
                        var sharedBar = new SharedBarsModel
                        {
                            SharedCount = sharedBars.Count,
                            BarName = myBar.Name,
                            City = myBar.City.Name,
                            Country = myBar.City.Country
                        };

                        results.Add(sharedBar);
                    }
                }
            }

            return results;
        }

        public async Task<UserModel> UserLogin(UserModel user)
        {
            // todo: best way to do case insentive checks in ef query?  ToUpperInvariant throwing exceptions
            var foundUser = await _secretBarClubContext.Users.SingleOrDefaultAsync(x =>
                x.Email.ToLower() == user.Email.ToLower() &&
                x.Password == user.Password);

            var model = _mapper.Map<UserModel>(foundUser);

            return model;
        }

        public async Task<UserModel> GetUserByEmail(string email)
        {
            var foundUser = await _secretBarClubContext.Users.SingleOrDefaultAsync(x => x.Email.ToLower() == email.Trim().ToLower());

            var model = _mapper.Map<UserModel>(foundUser);

            return model;
        }

        public async Task<UserModel> AddNewUser(UserModel user)
        {
            var newUser = _mapper.Map<User>(user);

            await _secretBarClubContext.Users.AddAsync(newUser);
            await _secretBarClubContext.SaveChangesAsync();

            return _mapper.Map<UserModel>(newUser);
        }
    }
}
