using System.Reflection;
using System.Text;
using System.Text.Json;
using FortCode.ConfigOptions;
using FortCode.Data;
using FortCode.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;

namespace FortCode
{
    public class Startup
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public virtual void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<SecretBarClubContext>();

            // todo: no need for the entire mvc stack here just the raw controllers are fine for this api... if all good then remove this comment
            // services.AddMvc();
            services.AddControllers();

            // Look for our automapper profiles in this assembly
            services.AddAutoMapper(Assembly.GetExecutingAssembly());

            // Add our own services
            services.AddScoped<ISecretBarClubService, SecretBarClubService>();

            // Bind the configurations options
            services.Configure<JwtSettingsOptions>(_configuration.GetSection(JwtSettingsOptions.JwtSettings));

            // Validation on incoming requests
            // todo: only used in testing fix this up 
            var rawKey = _configuration
                .GetSection(JwtSettingsOptions.JwtSettings)
                .Get<JwtSettingsOptions>().SecretKey;

            var key = Encoding.ASCII.GetBytes(rawKey);

            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = true;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });
        }

        public virtual void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory, SecretBarClubContext dbContext)
        {
            app.UseExceptionHandler(a => a.Run(async context =>
            {
                var exceptionHandlerPathFeature = context.Features.Get<IExceptionHandlerPathFeature>();
                var exception = exceptionHandlerPathFeature.Error;

                var result = JsonSerializer.Serialize(new { error = exception.Message });
                context.Response.ContentType = "application/json";
                await context.Response.WriteAsync(result);
            }));

            // todo: clean this up in PRD... make sure only EnsureCreated ran?  or apply migrations too? etc... figure that out btu for now seeding if no data in ALL environments
            SecretBarClubInitializer.Initialize(dbContext);

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app
                .UseExceptionHandler("/error")
                .UseFileServer()
                .UseRouting()
                .UseAuthentication()
                .UseAuthorization()
                .UseEndpoints(endPoints => { endPoints.MapControllers(); });
        }
    }
}
