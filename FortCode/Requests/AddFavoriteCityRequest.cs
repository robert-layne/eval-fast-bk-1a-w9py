﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FortCode.Requests
{
    public class AddFavoriteCityRequest
    {
        public string Name { get; set; }
        public string Country { get; set; }
    }
}
