﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FortCode.Requests
{
    public class AddFavoriteBarRequest
    {
        public string Name { get; set; }
        public string FavoriteDrinkName { get; set; }
        public int CityId { get; set; }
    }
}
