﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FortCode.ConfigOptions
{
    public class JwtSettingsOptions
    {
        public const string JwtSettings = "JwtSettings";

        public string SecretKey { get; set; }
    }
}
