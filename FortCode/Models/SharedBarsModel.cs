﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FortCode.Models
{
    public class SharedBarsModel
    {
        public int SharedCount { get; set; }
        public string BarName { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
    }
}
