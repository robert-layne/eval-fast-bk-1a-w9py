﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FortCode.Models
{
    // todo: convert this entire thing over to .net core identity instead?

    public class UserModel
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string? JwtToken { get; set; }

        public List<BarModel>? Bars { get; set; }
        public List<CityModel>? Cities { get; set; } = null!;
    }
}
