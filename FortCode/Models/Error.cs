﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FortCode.Models
{
    public class Error
    {
        public string Name { get; set; }
        public string Message { get; set; }
        public string StackTrace { get; set; }

        public Error(Exception ex)
        {
            Name = ex.GetType().Name;
            Message = ex.Message;
            StackTrace = ex.ToString();
        }
    }
}
