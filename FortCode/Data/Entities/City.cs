﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using FortCode.Models;

namespace FortCode.Data.Entities
{
    public class City
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Country { get; set; }
        public ICollection<Bar> Bars { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
    }
}
