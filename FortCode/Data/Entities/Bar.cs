﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Threading.Tasks;

namespace FortCode.Data.Entities
{
    public class Bar
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string FavoriteDrinkName { get; set; }
        public int CityId { get; set; }
        public City City { get; set; }
    }
}
