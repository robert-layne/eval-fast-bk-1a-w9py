﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FortCode.Data.Entities;
using FortCode.Models;
using FortCode.Requests;

namespace FortCode.Data
{
    public class MappingProfile: Profile
    {
        // todo: if this gets too big then break out into smaller profiles

        public MappingProfile()
        {
            CreateMap<AddFavoriteCityRequest, CityModel>();
            CreateMap<City, CityModel>()
                .ReverseMap();

            CreateMap<AddFavoriteBarRequest, BarModel>();
            CreateMap<Bar, BarModel>()
                .ReverseMap();

            CreateMap<User, UserModel>()
                .ReverseMap();

        }
    }
}
