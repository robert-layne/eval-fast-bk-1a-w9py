﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FortCode.Data.Entities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;


namespace FortCode.Data
{
    public class SecretBarClubContext: DbContext
    {
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _host;

        public DbSet<City> Cities { get; set; }
        public DbSet<Bar> Bars { get; set; }
        public DbSet<User> Users { get; set; }

        public SecretBarClubContext(IConfiguration configuration, IWebHostEnvironment host)
        {
            _configuration = configuration;
            _host = host;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // todo: for now all good w/out fluent api
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);

            // todo: change this code to work with only one key instead of doing an environmental check 
            string connectionString = (_host.IsDevelopment()
                ? _configuration["ConnectionStrings:DefaultConnection"]
                : Environment.GetEnvironmentVariable("FORTCODEENV_ConnectionStrings_DbContext")) ?? string.Empty;
            optionsBuilder.UseSqlServer(connectionString);
        }

    }
}
