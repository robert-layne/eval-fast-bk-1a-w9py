﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FortCode.Data.Entities;

namespace FortCode.Data
{
    public static class SecretBarClubInitializer
    {

        public static void Initialize(SecretBarClubContext dbContext)
        {
            // todo: clean this up in how it's called.. this should be better when in PRD
            dbContext.Database.EnsureCreated();

            // If we have any users in the DB then don't seed
            if (dbContext.Users.Any()) return;

            // Users
            var u1 = new User
            {
                Name = "Robert Layne",
                Email = "robert.layne@gmail.com",
                Password = "Let'sGetS0me"
            };

            var u2 = new User
            {
                Name = "Joni Layne",
                Email = "joni.layne@gmail.com",
                Password = "iLoveBobby12!"
            };

            dbContext.Users.AddRange(u1, u2);
            dbContext.SaveChanges();


            // Cities
            var c1 = new City
            {
                Name = "Wilmington",
                Country = "USA",
                User = u1
            };

            dbContext.Cities.Add(c1);
            dbContext.SaveChanges();

            var c2 = new City
            {
                Name = "Beaufort",
                Country = "USA",
                User = u1
            };

            dbContext.Cities.Add(c2);
            dbContext.SaveChanges();

            var c3 = new City
            {
                Name = "Las Vegas",
                Country = "USA",
                User = u1
            };

            dbContext.Cities.Add(c3);
            dbContext.SaveChanges();

            var c4 = new City
            {
                Name = "Paris",
                Country = "France",
                User = u2
            };

            dbContext.Cities.Add(c4);
            dbContext.SaveChanges();

            var c5 = new City
            {
                Name = "Wilmington",
                Country = "USA",
                User = u2
            };

            dbContext.Cities.Add(c5);
            dbContext.SaveChanges();

            // Bars
            var b1 = new Bar
            {
                Name = "The Tin Cap",
                FavoriteDrinkName = "Apple Cider",
                City = c1
            };

            dbContext.Bars.Add(b1);
            dbContext.SaveChanges();

            var b2 = new Bar
            {
                Name = "The Shack",
                FavoriteDrinkName = "Shrimp Cocktail",
                City = c2
            };

            dbContext.Bars.Add(b2);
            dbContext.SaveChanges();

            var b3 = new Bar
            {
                Name = "Wee Oi, Wee Oi",
                FavoriteDrinkName = "Baguettearitta",
                City = c4
            };

            dbContext.Bars.Add(b3);
            dbContext.SaveChanges();

            var b4 = new Bar
            {
                Name = "The Tin Cap",
                FavoriteDrinkName = "Apple Cider",
                City = c5
            };

            dbContext.Bars.Add(b4);
            dbContext.SaveChanges();
        }
    }
}
