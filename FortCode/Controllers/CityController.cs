﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using FortCode.ConfigOptions;
using FortCode.Models;
using FortCode.Requests;
using FortCode.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;

namespace FortCode.Controllers
{
    [Authorize]
    [Route("api/secret-bar-club/cities")]
    [ApiController]
    public class CityController : ControllerBase
    {
        private readonly ISecretBarClubService _service;
        private readonly IMapper _mapper;

        private int AuthenticatedUsersId => Convert.ToInt32(User.FindFirstValue(ClaimTypes.PrimarySid));

        public CityController(ISecretBarClubService secretBarClubService, IMapper mapper)
        {
            _service = secretBarClubService;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> GetFavoriteCities()
        {
            try
            {
                var cities = await _service.GetFavoriteCities(this.AuthenticatedUsersId);

                return Ok(cities);
            }
            catch (Exception e)
            {
                return BadRequest("Failed to get favorite cities.");
            }
        }

        [HttpPost]
        public async Task<IActionResult> AddFavoriteCity([FromBody] AddFavoriteCityRequest request)
        {
            try
            {
                var cities = await _service.GetFavoriteCities(AuthenticatedUsersId);
                if (cities.Any(x => x.Name.ToUpperInvariant() == request.Name.ToUpperInvariant() &&
                                    x.Country.ToUpperInvariant() == request.Country.ToUpperInvariant()))
                {
                    return BadRequest($"You already have city {request.Name} in {request.Country} as one of your favorites.");
                }

                var city = _mapper.Map<AddFavoriteCityRequest, CityModel>(request);

                var cityDto = await _service.AddFavoriteCity(city, AuthenticatedUsersId);

                return CreatedAtAction(nameof(AddFavoriteCity), new { id = cityDto.Id }, cityDto);
            }
            catch (Exception e)
            {
                return BadRequest("Failed to add a city.");
            }
        }

        [HttpDelete]
        [Route("{id:int:required}")]
        public async Task<IActionResult> DeleteFavoriteCity(int id)
        {
            try
            {
                var city = await _service.GetFavoriteCity(id, AuthenticatedUsersId);
                if (city == null)
                {
                    return NotFound($"Unable to find city with id {id} to delete.");
                }

                await _service.DeleteFavoriteCity(id, AuthenticatedUsersId);

                return NoContent();
            }
            catch (Exception e)
            {
                return BadRequest("Failed to delete a city.");
            }
        }

        [HttpGet]
        [Route("{id:int:required}/favorite-bars")]
        public async Task<IActionResult> GetFavoriteBarsByCity(int id)
        {
            try
            {
                var city = await _service.GetFavoriteCity(id, AuthenticatedUsersId);
                if (city == null)
                {
                    return NotFound($"Unable to find city with id {id} as your favorite.");
                }

                var cities = await _service.GetFavoriteBars(id, AuthenticatedUsersId);

                return Ok(cities);
            }
            catch (Exception e)
            {
                return BadRequest("Failed to find favorite bars by city.");
            }
        }
    }
}
