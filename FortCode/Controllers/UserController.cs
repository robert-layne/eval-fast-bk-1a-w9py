﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using FortCode.ConfigOptions;
using FortCode.Helpers;
using FortCode.Models;
using FortCode.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace FortCode.Controllers
{
    [Route("api/secret-bar-club/users")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly ISecretBarClubService _service;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;

        private string JwtSecretKey => _configuration
            .GetSection(JwtSettingsOptions.JwtSettings)
            .Get<JwtSettingsOptions>().SecretKey;

        public UserController(ISecretBarClubService secretBarClubService, IMapper mapper, IConfiguration configuration )
        {
            _service = secretBarClubService;
            _mapper = mapper;
            _configuration = configuration;
        }

        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Login([FromBody] UserModel user)
        {
            try
            {
                var foundUser = await _service.UserLogin(user);
                if (foundUser == null)
                {
                    return BadRequest("Login failed, please check your credentials or create a new account via /users/create");
                }

                // User is logged in create the token
                foundUser.JwtToken = JwtHelper.CreateToken(JwtSecretKey, foundUser);

                return Ok(new
                {
                    email = foundUser.Email,
                    name = foundUser.Name,
                    jwtToken = foundUser.JwtToken
                });
            }
            catch (Exception e)
            {
                return BadRequest("Falied to Login.");
            }
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("add")]
        public async Task<IActionResult> CreateNewUser([FromBody] UserModel user)
        {
            try
            {
                var foundUser = await _service.GetUserByEmail(user.Email);
                if (foundUser != null)
                {
                    return BadRequest($"Unable to create user, '{user.Email}' already exists");
                }

                // todo: stop the going back and forth from entity's to model objects and keep them tracked instead? here and everywhere?
                var model = _mapper.Map<UserModel>(user);

                var newUser = await _service.AddNewUser(model);

                return CreatedAtAction(nameof(CreateNewUser), new { id = newUser.Id }, newUser);

            }
            catch (Exception e)
            {
                return BadRequest("Failed to create a new user.");
            }
        }
    }
}
