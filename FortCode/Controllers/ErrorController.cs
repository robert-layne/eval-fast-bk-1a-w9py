﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using FortCode.Models;
using Microsoft.AspNetCore.Diagnostics;

namespace FortCode.Controllers
{
    [ApiController]
    public class ErrorController : ControllerBase
    {
        [Route("error")]
        public Error Error()
        {
            var context = HttpContext.Features.Get<IExceptionHandlerFeature>();
            var exception = context.Error;

            Response.StatusCode = (int)HttpStatusCode.InternalServerError;

            return new Error(exception);
        }
    }
}
