﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using FortCode.ConfigOptions;
using FortCode.Models;
using FortCode.Requests;
using FortCode.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;


namespace FortCode.Controllers
{
    [Authorize]
    [Route("api/secret-bar-club/bars")]
    [ApiController]
    public class BarController : ControllerBase
    {
        private readonly ISecretBarClubService _service;
        private readonly IMapper _mapper;

        private int AuthenticatedUsersId => Convert.ToInt32(User.FindFirstValue(ClaimTypes.PrimarySid));

        // todo: abstract out any validation on requests to a validation interface/layer
        // todo: add logging on exceptions

        public BarController(ISecretBarClubService cityService, IMapper mapper)
        {
            _service = cityService;
            _mapper = mapper;
        }

        [HttpPost]
        public async Task<IActionResult> AddFavoriteBar([FromBody] AddFavoriteBarRequest request)
        {
            try
            {
                var city = await _service.GetFavoriteCity(request.CityId, AuthenticatedUsersId);
                if (city == null)
                {
                    return NotFound($"Unable to find city with id {request.CityId} as your favorite, add this city first before you can add bars to it.");
                }

                var bars = await _service.GetFavoriteBars(request.CityId, AuthenticatedUsersId);
                var hasBar = bars.Any(x => x.Name.ToUpperInvariant() == request.Name.ToUpperInvariant() &&
                                           x.FavoriteDrinkName.ToUpperInvariant() == request.FavoriteDrinkName.ToUpperInvariant());

                if (hasBar)
                {
                    return BadRequest($"You have already favorited the drink {request.FavoriteDrinkName} at {request.Name} in this city.");
                }

                var bar = _mapper.Map<AddFavoriteBarRequest, BarModel>(request);

                var barDto = await _service.AddFavoriteBar(bar, request.CityId, AuthenticatedUsersId);

                return CreatedAtAction(nameof(AddFavoriteBar), new { id = barDto.Id }, barDto);
            }
            catch (Exception e)
            {
                return BadRequest("Failed to add favoite bar");
            }
        }

        [HttpDelete]
        [Route("{id:required}")]
        public async Task<IActionResult> DeleteFavoriteBar(int id)
        {
            try
            {
                var bar = await _service.GetFavoriteBar(id, AuthenticatedUsersId);
                if (bar == null)
                {
                    return NotFound($"Unable to find bar with id {id} to delete.");
                }

                await _service.DeleteFavoriteBar(id, AuthenticatedUsersId);

                return NoContent();
            }
            catch (Exception e)
            {
                return BadRequest("Failed to delete a bar");
            }
        }

        [HttpGet]
        [Route("shared-favorites")]
        public async Task<IActionResult> GetFavoritedBarsShared()
        {
            try
            {
                var cities = await _service.GetFavoriteBarsShared(AuthenticatedUsersId);

                return Ok(cities);
            }
            catch (Exception e)
            {
                return BadRequest("Failed to get shared favorites");
            }
        }
    }
}
