# Explanations

- Decided to go with three separate controllers controlling the main domains of `City`, `Bar`, and `User`
- I would def wrap this in API versioning in real project (using .net core) so you can have `/v1/` and `/v2/` version
- I went with shoving everything in one abstraction in a `SecretBarClubService` worker class.  This class interacts with the `DbContext`
    - I would prob split this out like the controllers and have a 1:1 controller:service class in real project
    - I did not abstract the `DbContext` into a repository although in a real project I probably would with generic Add/Update/Delete/etc methods and such for entitites
- I created an `Error` controller and injected that into the pipeline to help with errors as I was coding/debugging
- The Entity Framework code and model is pretty weak - Ideally, we should have some join/associations for some many-to-many relationships, only have one conceptual city (but we're missing states), etc... 
    - For this I just layed it out flat and easy and went with that
    - I would def refactor this in real application
    - wasn't sure how to get the injected env variable from Docker to be the same key as the appsettings.json file.  Ideally, you would want only *one* key and have it overridden in environments
- Using `JwtTokens` for Authentication/Authorization  and reading the logged in user from the identity claims on the `Controllers`.
- Using config options to bind/read the appsettings.json
- I quickly used a simple `User` class with exposed passwords saved in the DB (**BAD!**) and all of that - that would never go into PRD and real code and instead would have `User` inherit from `Identity` and use that or whatever other login infrastructure is in place
- Def want all the DB entities in their own class `Entities` here and then use some other classes to pass the data around.  Usually named called `DTO's`, `Models`, etc.. 
    - Right now this is a little messy in that it's a mix between `Request` and `Model` objects, I would want to clean this up and either move to all `Models` or a `Request/Response` naming convention
- I did not complete all of the testing requirements 
    - do not have daily exp with testing and never done TDD
    - do not have any integration tests with EF DB
    - if forced to finish this in real PRD app... would ideally like to have unit tests on controllers, the main service class, and integration tests with EF
- Dapper is new to me - that was really neat to research and see in action


