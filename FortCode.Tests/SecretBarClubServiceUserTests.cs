using System;
using System.Threading.Tasks;
using AutoMapper;
using FortCode.Data;
using FortCode.Data.Entities;
using FortCode.Models;
using FortCode.Services;
using FortCode.Tests.Mock;
using Microsoft.EntityFrameworkCore.Internal;
using Xunit;

namespace FortCode.Tests
{
    public class SecretBarClubServiceUserTests
    {
        // todo: build out more here manually? or use some mock/fake/etc... framework?

        private SecretBarClubServiceMock MockService => new SecretBarClubServiceMock();

        [Fact]
        public async Task UserLogin_should_return_user_when_user_exists()
        {
            // User is in the mock data
            var user = new UserModel
            {
                Email = "robert.layne@gmail.com",
                Password = "Let'sGetS0me"
            };

            var model = await MockService.UserLogin(user);

            Assert.Equal(1, model.Id);
        }

        [Fact]
        public async Task UserLogin_should_return_null_when_user_not_matched()
        {
            // User is in the mock data
            var user = new UserModel
            {
                Email = "robert.layne@gmail.com",
                Password = "wrong-password"
            };

            var model = await MockService.UserLogin(user);

            Assert.Null(model);
        }

        // ...
    }
}
