﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using FortCode.Data;
using FortCode.Data.Entities;
using FortCode.Models;
using FortCode.Services;

namespace FortCode.Tests.Mock
{
    public class SecretBarClubServiceMock : ISecretBarClubService
    {
        // todo: this entire class is sync but cool for now since only used for mocking unit tests, revist if becomes slow or if have better solution then wrapping everything in Task.Run(() => so stuff);

        private static List<CityModel> _cities = new List<CityModel>();
        private static List<UserModel> _users = new List<UserModel>();
        private static List<BarModel> _bars = new List<BarModel>();

        public SecretBarClubServiceMock()
        {
            if (!_cities.Any() ||
                !_bars.Any() ||
                !_users.Any())
            {
                Init();
            }
        }

        public Task<CityModel> AddFavoriteCity(CityModel city, int userId)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteFavoriteCity(int cityId, int userId)
        {
            throw new NotImplementedException();
        }

        public Task<List<CityModel>> GetFavoriteCities(int userId)
        {
            throw new NotImplementedException();
        }

        public Task<CityModel> GetFavoriteCity(int cityId, int userId)
        {
            throw new NotImplementedException();
        }

        public Task<List<BarModel>> GetFavoriteBars(int cityId, int userId)
        {
            throw new NotImplementedException();
        }

        public Task<BarModel> GetFavoriteBar(int barId, int userId)
        {
            throw new NotImplementedException();
        }

        public Task<BarModel> AddFavoriteBar(BarModel bar, int cityId, int userId)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteFavoriteBar(int barId, int userId)
        {
            throw new NotImplementedException();
        }

        public Task<List<SharedBarsModel>> GetFavoriteBarsShared(int userId)
        {
            throw new NotImplementedException();
        }

        public async Task<UserModel> UserLogin(UserModel user)
        {
            var foundUser = _users.SingleOrDefault(x => x.Email.ToLower() == user.Email.ToLower() &&
                                                        x.Password == user.Password);
            return foundUser;
        }

        public Task<UserModel> GetUserByEmail(string email)
        {
            throw new NotImplementedException();
        }

        public Task<UserModel> AddNewUser(UserModel user)
        {
            throw new NotImplementedException();
        }

        private void Init()
        {
            // Users
            var u1 = new UserModel
            {
                Id = 1,
                Name = "Robert Layne",
                Email = "robert.layne@gmail.com",
                Password = "Let'sGetS0me"
            };

            _users.Add(u1);
        }
    }
}
