# API Details

This API uses JWT tokens and you first need to:
* Create a new account at **/users/add**
* Login with those credentials at **/users/login**
* Use the provided `jwtToken` value provided to you in the Login response as the `Bearer Token` for all other the other calls

## Summary:
1. POST api/secret-bar-club/**users**/login
2. POST    api/secret-bar-club/**users**/add
3. GET     api/secret-bar-club/**cities** 
4. POST    api/secret-bar-club/**cities**
5. DELETE  api/secret-bar-club/**cities**/:cityId
6. GET     api/secret-bar-club/**cities**/favorite-bars
7. POST    api/secret-bar-club/**bars**
8. DELETE  api/secret-bar-club/**bars**/:barId
9. GET     api/secret-bar-club/**bars**/shared-favorites

Docker Container Sample URL:  `http://localhost:8100/api/secret-bar-club/bars/shared-favorites`

# Examples
## **Users**
* **POST**    api/secret-bar-club/**users**/**login**

    Sample Request:

    ```yaml
    {
        "email": "robert.layne@gmail.com",
        "password": "h0wDidIdo?"
    }
    ```
    
    Sample Response:

    ```yaml
    {
        "email": "robert.layne@gmail.com",
        "name": "Robert Layne",
        "jwtToken": "your-jwt-token-is-here"
    }
    ```

* **POST**    api/secret-bar-club/**users**/**add**

    Sample Request:

    ```yaml
    {    
        "name": "Robert Layne", 
        "email": "robert.layne@gmail.com",
        "password": "h0wDidIdo?"
    }
    ```

    Sample Response:

    ```yaml
    {
        "id": 1002,
        "name": "Robert Layne",
        "email": "robert.layne@gmail.com",
        "password": null,
        "jwtToken": "your-jwt-token-is-here",
        "bars": null,
        "cities": null
    }
    ```


## **Cities**
* **GET**     api/secret-bar-club/**cities** 

    Sample Request:

    ```yaml
    N/A
    ```

    Sample Response:

    ```yaml
    [
        {
            "id": 4,
            "name": "Paris",
            "country": "France"
        },
        {
            "id": 5,
            "name": "Wilmington",
            "country": "USA"
        }
    ]
    ```

* **POST**    api/secret-bar-club/**cities**

    Sample Request:

    ```yaml
    {
        "name": "Boston",
        "country": "USA"
    }
    ```
    Sample Response:

    ```yaml
    {
        "id": 1002,
        "name": "Boston",
        "country": "USA"
    }
    ```

* **DELETE**  api/secret-bar-club/**cities**/:cityId

    Sample Request:

    ```yaml
    N/A - pass in :cityId via url
    ```

    Sample Response:

    ```yaml
    N/A - http 204 No Content if success
    ```

* **GET**     api/secret-bar-club/**cities**/**favorite-bars**

    Sample Request:

    ```yaml
    N/A - pass in :cityId via url
    ```

    Sample Response:

    ```yaml
    [
        {
            "id": 4,
            "name": "The Tin Cap",
            "favoriteDrinkName": "Apple Cider"
        },
        {
            "id": 1002,
            "name": "The Tin Cap",
            "favoriteDrinkName": "Fake Coors Light"
        }
    ]
    ```

## **Bars**
* **POST**    api/secret-bar-club/**bars**

    Sample Request:

    ```yaml
    {
        "name": "The Overlook Hotel",
        "favoriteDrinkName": "Gin Martini",
        "cityId": 1003
    }
    ```
    Sample Response:

    ```yaml
    {
        "id": 56    
        "name": "The Overlook Hotel",
        "favoriteDrinkName": "Gin Martini",
        "cityId": 1003
    }
    ```

* **DELETE**  api/secret-bar-club/**bars**/:barId

    Sample Request:

    ```yaml
    N/A - pass in :barId via url
    ```

    Sample Response:

    ```yaml
    N/A - http 204 No Content if success
    ```

* **GET**     api/secret-bar-club/**bars**/**shared-favorites**

    Sample Request:

    ```yaml
    N/A
    ```

    Sample Response:

    ```yaml
    [
        {
            "sharedCount": 2,
            "barName": "The Tin Cap",
            "city": "Wilmington",
            "country": "USA"
        }
    ]
    ```